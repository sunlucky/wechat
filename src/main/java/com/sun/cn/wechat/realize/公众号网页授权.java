package com.sun.cn.wechat.realize;

import com.sun.cn.wechat.util.HttpClientUtil;
import org.json.JSONArray;
import org.json.JSONObject;

//公众号网页授权

public class 公众号网页授权 {
	
	//以下信息建议单独做成配置文件
	private static String APPID="wxcd52b51dd214515e";			//公众号开发者ID
	private static String APPKEY="293e49419c1b51cee76b35f321f3ec44";	    //公众号开发者密码
	private static String CODE="041AOh0000rkkN1Vjc2001dcVa4AOh0a"; 	//code

	//调用示例
	public static void main(String[] args) {
		JSONObject result=getToken(CODE);
		String access_token=result.getString("access_token");
		String openid=result.getString("openid");
	}

	//通过code换取网页授权access_token
	public static JSONObject getToken(String code) {
		JSONObject result=new JSONObject();
		String url="https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code";
		// 参数
		StringBuffer params = new StringBuffer();
		try {
			params.append("&appid="+APPID);
			params.append("&secret="+APPKEY);
			params.append("&code="+code);
			result= HttpClientUtil.doGet(url,params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
