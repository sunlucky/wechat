package com.sun.cn.wechat.realize;

import com.sun.cn.wechat.util.HttpClientUtil;
import org.json.JSONArray;
import org.json.JSONObject;

//微信模板消息

public class 公众号模板消息 {
	
	//以下信息建议单独做成配置文件
	private static String APPID="wxcd52b51dd214515e";			//公众号开发者ID
	private static String APPKEY="293e49419c1b51cee76b35f321f3ec44";	    //公众号开发者密码
	private static String template_id="L7ZjwDJGBdssG1kKtrcPPYSYydTJrH86SSORRtWa8Ik"; 	//模板ID
	private static String appid=""; 		//跳转小程序开发者ID
	private static String pagepath=""; 		//跳转小程序页面
	
	//调用示例
	public static void main(String[] args) {
		JSONObject json=new JSONObject();
		json.put("template_id", template_id);
		JSONObject miniprogram=new JSONObject();
		miniprogram.put("appid", appid);
		miniprogram.put("pagepath", pagepath);
		json.put("miniprogram", miniprogram);
		//以下内容模板信息，视业务需求决定
		JSONObject first=new JSONObject();
		first.put("value", "你好，你上报的仪器审核成功！");
		first.put("color", "#173177");
		JSONObject keyword1=new JSONObject();
		keyword1.put("value", "气体透过率测试仪");
		keyword1.put("color", "#173177");
		JSONObject keyword2=new JSONObject();
		keyword2.put("value", "2022-02-16 10:22:33");
		keyword2.put("color", "#173177");
		JSONObject keyword3=new JSONObject();
		keyword3.put("value", "44.50（万元）");
		keyword3.put("color", "#173177");
		JSONObject remark=new JSONObject();
		remark.put("value", "请浏览器打开http://www.ahky.cn查看详情");
		remark.put("color", "#173177");
		JSONObject data=new JSONObject();
		data.put("first", first);
		data.put("name", keyword1);
		data.put("time", keyword2);
		data.put("amount", keyword3);
		data.put("remark", remark);
		json.put("data", data);
		
		String token=getToken();
		if(token!=null&&!token.isEmpty()) {
			JSONArray openids=getAllUser(token);
			for(Object openid:openids) {
				json.put("touser", openid);
				String reuslt=sendMessage(token,json);
				if(reuslt.equals("ok")) {
					System.out.println("发送成功！");
				}
			}
		}
		
	}

	//获取TOKEN
	public static String getToken() {
		String token=null;
		String url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
		// 参数
		StringBuffer params = new StringBuffer();
		try {
			params.append("&appid=" + APPID);
			params.append("&secret="+APPKEY);
			JSONObject result= HttpClientUtil.doGet(url,params);
			token=result.getString("access_token");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return token;
	}
	
	//获取已订阅用户OPENID
	public static JSONArray getAllUser(String token) {
		JSONArray openids=null;
		String url="https://api.weixin.qq.com/cgi-bin/user/get?access_token=";
		// 参数
		StringBuffer params = new StringBuffer();
		try {
			params.append(token);
			JSONObject result= HttpClientUtil.doGet(url,params);
			openids=result.getJSONObject("data").getJSONArray("openid");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return openids;
	}
	
	//发送模板消息
	public static String sendMessage(String token,JSONObject json) {
		String resultstr=null;
		String url="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";
		// 参数
		StringBuffer params = new StringBuffer();
		try {
			params.append(token);
			JSONObject result=HttpClientUtil.doPost(url,params,json);
			resultstr=result.getString("errmsg");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultstr;
	}

}
