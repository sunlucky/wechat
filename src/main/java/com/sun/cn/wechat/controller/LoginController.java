package com.sun.cn.wechat.controller;

import com.sun.cn.wechat.realize.公众号网页授权;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class LoginController {

    @ResponseBody
    @GetMapping("/")
    public String login(String code) {
        log.info("获取到code:"+code);
        JSONObject result=公众号网页授权.getToken(code);
        return "获取到openid+"+result.getString("openid");
    }

}
